from setuptools import setup, find_packages
from PyQt5 import uic
import os

for folder, folders, files in os.walk('src'):
    for fn in files:
        if fn.endswith('.ui'):
            filename=os.path.join(folder, fn)
            with open(filename.rsplit('.', 1)[0]+'_ui.py', 'wt') as f:
                uic.compileUi(filename, f)


setup(
    name='capillarysizer',
    version='0.0.1',
    packages=find_packages('src'),
    install_requires=['numpy', 'h5py', 'matplotlib', 'click', 'scipy'],
    entry_points={'gui_scripts': ['capillarysizer = capillarysizer.__main__:run']},
    package_data={'': ['*.ui']},
    package_dir={'':'src'},
    url='http://gitlab.com/awacha/capillarysizer',
    license='BSD',
    author='András Wacha',
    author_email='awacha@gmail.com',
    description='Determine capillary position and thickness from a scan at a SAXS instrument'
)
