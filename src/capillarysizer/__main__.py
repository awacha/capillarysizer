# coding=utf-8

"""Main entry point"""
import sys

from PyQt5 import QtWidgets, QtCore, QtGui
from .capillarysizer import CapillarySizer

import click


@click.command('capillarysizer')
@click.option('-f', 'nexusfile', help='The name of the NeXuS scan file to load', required=False,
              type=click.Path(exists=True, file_okay=True, dir_okay=False, writable=False, readable=True))
@click.version_option()
def run(nexusfile: str = None):
    app = QtWidgets.QApplication(sys.argv)
    mainwin = CapillarySizer()
    if nexusfile is not None:
        mainwin.nexusfile = nexusfile
        mainwin.nexusFileLineEdit.setText(nexusfile)
        mainwin.updateDatasetComboBox()

    mainwin.show()
    result = app.exec_()
    mainwin.destroy()
    mainwin.deleteLater()
    app.deleteLater()
    sys.exit(result)
