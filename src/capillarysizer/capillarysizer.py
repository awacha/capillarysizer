# coding=utf-8
"""Capillary size determination utility"""

from typing import Optional, List

import numpy as np
import h5py
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import pyqtSignal as Signal, pyqtSlot as Slot
import scipy.odr
import scipy.signal
from .capillarysizer_ui import Ui_Form


class CapillarySizer(QtWidgets.QWidget, Ui_Form):
    figure: Figure
    axes: Axes
    canvas: FigureCanvasQTAgg
    figtoolbar: NavigationToolbar2QT
    nexusfile: Optional[str] = None

    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        self.figure = Figure()
        self.axes = self.figure.add_subplot(1, 1, 1)
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.browseToolButton.clicked.connect(self.onBrowseClicked)
        self.figureVerticalLayout.addWidget(self.figtoolbar)
        self.figureVerticalLayout.addWidget(self.canvas)
        self.derivativePushButton.toggled.connect(self.onDerivativeToggled)
        self.dataNameComboBox.currentTextChanged.connect(self.onCurrentDatasetChanged)
        self.negativeValDoubleSpinBox.valueChanged.connect(self.onNegativeInflexionPointValueChanged)
        self.negativeErrDoubleSpinBox.valueChanged.connect(self.onNegativeInflexionPointErrorChanged)
        self.positiveValDoubleSpinBox.valueChanged.connect(self.onPositiveInflexionPointValueChanged)
        self.positiveErrDoubleSpinBox.valueChanged.connect(self.onPositiveInflexionPointErrorChanged)
        self.fitSymmetricNegativeToolButton.clicked.connect(self.onFit)
        self.fitSymmetricPositiveToolButton.clicked.connect(self.onFit)
        self.fitAsymmetricNegativeToolButton.clicked.connect(self.onFit)
        self.fitAsymmetricPositiveToolButton.clicked.connect(self.onFit)
        self.axesNameComboBox.currentTextChanged.connect(self.onXAxisNameChanged)
        self.signalNameComboBox.currentTextChanged.connect(self.onYAxisNameChanged)
        self.normalizationNameComboBox.currentTextChanged.connect(self.onNormalizationNameChanged)
        self.normalizeCheckBox.toggled.connect(self.onNormalizeChanged)
        self.bsplineDegreeSpinBox.valueChanged.connect(self.onSplineDegreeChanged)
        self.seqNumSpinBox.valueChanged.connect(self.onSeqNumChanged)

    @Slot(name='onBrowseClicked')
    def onBrowseClicked(self):
        fn, fltr = QtWidgets.QFileDialog.getOpenFileName(self, 'Select a capillary scan file', '',
                                                         'NeXuS files (*.nxs);;All files (*)', 'NeXuS files (*.nxs)')
        if not fn:
            return
        self.nexusfile = fn
        self.updateDatasetComboBox()
        self.nexusFileLineEdit.setText(self.nexusfile)

    @Slot(int, name='onSeqNumChanged')
    def onSeqNumChanged(self, value: int):
        try:
            self.nexusfile = self.nexusFileLineEdit.text() % value
        except TypeError:
            self.nexusfile = self.nexusFileLineEdit.text().format(value)
        self.updateDatasetComboBox()

    @Slot(bool, name='onDerivativeToggled')
    def onDerivativeToggled(self, checked: bool):
        self.plot()

    @Slot(str, name='onCurrentDatasetChanged')
    def onCurrentDatasetChanged(self, currenttext: str):
        originaltext = {}
        for cbox in [self.axesNameComboBox, self.signalNameComboBox, self.normalizationNameComboBox]:
            originaltext[cbox] = cbox.currentText()
            cbox.blockSignals(True)
            cbox.clear()
        try:
            if self.nexusfile is None:
                return
            if self.dataNameComboBox.currentIndex() < 0:
                return
            with h5py.File(self.nexusfile, 'r') as h5:
                group = h5[currenttext]
                members = sorted([name for name in group if isinstance(group[name], h5py.Dataset)])
                self.axesNameComboBox.addItems(members)
                self.signalNameComboBox.addItems(members)
                self.normalizationNameComboBox.addItems(members)
        finally:
            for cbox in [self.axesNameComboBox, self.signalNameComboBox, self.normalizationNameComboBox]:
                cbox.setCurrentIndex(-1)
                cbox.blockSignals(False)
                index = cbox.findText(originaltext[cbox])
                if index >= 0:
                    cbox.setCurrentIndex(index)

    @Slot(float, name='onNegativeInflexionPointValueChanged')
    def onNegativeInflexionPointValueChanged(self, value: float):
        self.recalculateThicknessAndPosition()

    @Slot(float, name='onNegativeInflexionPointErrorChanged')
    def onNegativeInflexionPointErrorChanged(self, value: float):
        self.recalculateThicknessAndPosition()

    @Slot(float, name='onPositiveInflexionPointValueChanged')
    def onPositiveInflexionPointValueChanged(self, value: float):
        self.recalculateThicknessAndPosition()

    @Slot(float, name='onPositiveInflexionPointErrorChanged')
    def onPositiveInflexionPointErrorChanged(self, value: float):
        self.recalculateThicknessAndPosition()

    @Slot(name='onXAxisNameChanged')
    def onXAxisNameChanged(self):
        self.plot()

    @Slot(name='onYAxisNameChanged')
    def onYAxisNameChanged(self):
        self.plot()

    @Slot(name='onNormalizationNameChanged')
    def onNormalizationNameChanged(self):
        self.plot()

    @Slot(bool, name='onNormalizeChanged')
    def onNormalizeChanged(self, checked: bool):
        self.plot()

    @Slot(int, name='onSplineDegreeChanged')
    def onSplineDegreeChanged(self, value: int):
        self.plot()

    def recalculateThicknessAndPosition(self):
        pos = self.positiveValDoubleSpinBox.value(), self.positiveErrDoubleSpinBox.value()
        neg = self.negativeValDoubleSpinBox.value(), self.negativeErrDoubleSpinBox.value()
        self.newThicknessLabel.setText(
            f'{abs(pos[0] - neg[0]) / 10.0:.4f} \xb1 {(pos[1] ** 2 + neg[1] ** 2) ** 0.5 / 10.0:.4f} cm')
        self.newPositionLabel.setText(
            f'{0.5 * (pos[0] + neg[0]):.4f} \xb1 {0.5 * (pos[1] ** 2 + neg[1] ** 2) ** 0.5:.4f} mm')

    @Slot(name='onFit')
    def onFit(self):
        if self.sender() in [self.fitSymmetricNegativeToolButton, self.fitAsymmetricNegativeToolButton]:
            sign = -1
        elif self.sender() in [self.fitSymmetricPositiveToolButton, self.fitAsymmetricPositiveToolButton]:
            sign = +1
        else:
            raise ValueError('This function must be called as a slot')
        try:
            x, y, axesname, signalname = self.getData()
        except (ValueError, TypeError):
            return
        xmin, xmax, ymin, ymax = self.axes.axis()
        idx = np.logical_and(
            np.logical_and(
                np.logical_and(x >= xmin, x <= xmax),
                np.logical_and(y >= ymin, y <= ymax)),
            np.logical_and(
                np.isfinite(x),
                np.isfinite(y)
            )
        )
        if idx.sum() < 5:
            QtWidgets.QMessageBox.critical(self, 'Error', 'Cannot fit: not enough points')
            return
        data = scipy.odr.RealData(x[idx], sign * y[idx])
        model = scipy.odr.Model(self.SymmetricGaussianODRFunction)
        odr = scipy.odr.ODR(data, model, [data.y.max(), 0.5 * (data.x.min() + data.x.max()), 0, 0.3 * data.x.ptp()],
                            ifixb=[1, 1, 0, 1])
        odrresult = odr.run()
        odr = scipy.odr.ODR(data, model, odrresult.beta, ifixb=[1, 1, 0, 1])
        odrresult = odr.run()
        if self.sender() in [self.fitAsymmetricPositiveToolButton, self.fitAsymmetricNegativeToolButton]:
            model = scipy.odr.Model(self.AsymmetricGaussianODRFunction)
            odr = scipy.odr.ODR(data, model, odrresult.beta.tolist() + [odrresult.beta[-1]], ifixb=[1, 1, 0, 1, 1])
            odrresult = odr.run()
        xfit = np.linspace(data.x.min(), data.x.max(), 300)
        yfit = sign * model.fcn(odrresult.beta, xfit)
        self.axes.plot(xfit, yfit, color='b' if sign < 0 else 'r')
        self.canvas.draw_idle()
        if sign < 0:
            self.negativeValDoubleSpinBox.setValue(odrresult.beta[1])
            self.negativeErrDoubleSpinBox.setValue(odrresult.sd_beta[1])
        else:
            assert sign > 0
            self.positiveErrDoubleSpinBox.setValue(odrresult.sd_beta[1])
            self.positiveValDoubleSpinBox.setValue(odrresult.beta[1])

    @staticmethod
    def AsymmetricGaussianODRFunction(beta, x):
        height, center, baseline, sigmaleft, sigmaright = beta
        idxleft = x <= center
        idxright = x > center
        y = np.empty_like(x)
        y[idxleft] = baseline + height * np.exp(-(x[idxleft] - center) ** 2 / (2 * sigmaleft ** 2))
        y[idxright] = baseline + height * np.exp(-(x[idxright] - center) ** 2 / (2 * sigmaright ** 2))
        return y

    @staticmethod
    def SymmetricGaussianODRFunction(beta, x):
        height, center, baseline, sigma = beta
        return CapillarySizer.AsymmetricGaussianODRFunction([height, center, baseline, sigma, sigma], x)

    @staticmethod
    def AsymmetricLorentzianODRFunction(beta, x):
        height, center, baseline, gammaleft, gammaright = beta
        idxleft = x <= center
        idxright = x > center
        y = np.empty_like(x)
        y[idxleft] = baseline + height / (1 + ((x[idxleft] - center) / gammaleft) ** 2)
        y[idxright] = baseline + height / (1 + ((x[idxright] - center) / gammaright) ** 2)
        return y

    @staticmethod
    def SymmetricLorentzianODRFunction(beta, x):
        height, center, baseline, gamma = beta
        return CapillarySizer.AsymmetricLorentzianODRFunction([height, center, baseline, gamma, gamma], x)

    def updateDatasetComboBox(self):
        currenttext = self.dataNameComboBox.currentText()
        self.dataNameComboBox.blockSignals(True)
        try:
            self.dataNameComboBox.clear()
            if self.nexusfile is None:
                return
            try:
                with h5py.File(self.nexusfile, 'r') as h5:
                    paths = list(self.findNXdataPaths(h5))
                    self.dataNameComboBox.addItems(paths)
            except Exception as exc:
                QtWidgets.QMessageBox.critical(self, 'Error', f'Error while reading NeXuS file: {exc}')
        finally:
            self.dataNameComboBox.setCurrentIndex(-1)
            self.dataNameComboBox.blockSignals(False)
        index = self.dataNameComboBox.findText(currenttext)
        if index >= 0:
            self.dataNameComboBox.setCurrentIndex(index)
        else:
            self.dataNameComboBox.setCurrentIndex(0)

    @staticmethod
    def findNXdataPaths(h5group: h5py.Group) -> List[str]:
        for name in sorted(h5group):
            if isinstance(h5group[name], h5py.Group) and ('NX_class' in h5group[name].attrs) and \
                    (h5group[name].attrs['NX_class'] == 'NXdata'):
                yield h5group[name].name
            if isinstance(h5group[name], h5py.Group):
                for path in CapillarySizer.findNXdataPaths(h5group[name]):
                    yield path

    def plot(self):
        try:
            self.axes.clear()
            if (self.nexusfile is None) or (self.dataNameComboBox.currentIndex() < 0):
                return
            try:
                x, y, axesname, signalname = self.getData()
            except (ValueError, TypeError):
                return
            self.axes.plot(x, y, 'k.-')
            self.axes.set_xlabel(axesname)
            self.axes.set_ylabel(signalname)
            self.axes.grid(True, which='both')
        finally:
            self.figtoolbar.update()
            self.canvas.draw_idle()

    def getData(self):
        with h5py.File(self.nexusfile, 'r') as h5:
            nxdata = h5[self.dataNameComboBox.currentText()]
            axesname = self.axesNameComboBox.currentText()
            signalname = self.signalNameComboBox.currentText()
            normalizationname = self.normalizationNameComboBox.currentText()
            x = np.asarray(nxdata[axesname])
            y = np.asarray(nxdata[signalname])
            try:
                norm = np.asarray(nxdata[normalizationname])
            except (KeyError, ValueError):
                norm = np.ones_like(y)
            if self.normalizeCheckBox.isChecked():
                y = y / norm
            if self.bsplineDegreeSpinBox.value() > 0:
                try:
                    y = scipy.signal.savgol_filter(y, self.bsplineDegreeSpinBox.value() * 2 - 1,
                                                   1 if self.bsplineDegreeSpinBox.value() > 1 else 0)
                except Exception as exc:
                    print(exc)
            if self.derivativePushButton.isChecked():
                y = (y[1:] - y[:-1]) / (x[1:] - x[:-1])
                x = 0.5 * (x[1:] + x[:-1])
                signalname = f'Derivative of {signalname}'
            return x, y, axesname, signalname
